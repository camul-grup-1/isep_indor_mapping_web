import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import CardHeader from '@material-ui/core/CardHeader';
import TextField from '@material-ui/core/TextField';
import { Box, IconButton } from '@material-ui/core';

const useStyles = makeStyles({
    box: {
        display: "flex",
        marginRight: "40px",
        marginBottom: "40px"
    },
    bottomLeftBox: {
        justifyContent: "flex-end",
        alignItems: "flex-end"
    },
    centerBox: {
        justifyContent: "center",
        alignItems: "center"
    },
    letters: {
        fontSize: "30px",
        lineHeight: "30px",
        fontWeight: "bold",
        color: "gray",
        marginRight: "30px"
    }
});


export default function Login() {

    const classes = useStyles();
 
    return (
        <Grid container spacing={0} direction="column" alignItems="center" justify="center" style={{ minHeight: '100vh' }}>
            <Grid item>
                <Card style={{minWidth: '75vw', minHeight: '75vh', backgroundColor: '#D3D3D3'}}>
                    <CardHeader style={{backgroundColor: '#73ADBE', height: '10vh'}} action={
                        <Grid container spacing={0} direction="row" style={{ minHeight: '100%' }}>
                            <Grid item>
                                <IconButton>
                                    <img src="/resources/portugal.png" alt="Logo" style={{ height: '9vh' }} />
                                </IconButton>
                            </Grid>
                            <Grid item>
                                <IconButton>
                                    <img src="/resources/english.png" alt="Logo" style={{ height: '10vh' }} />
                                </IconButton>
                            </Grid>
                        </Grid>
                    }>
                    </CardHeader>
                    <CardContent>
                    <Grid container spacing={0} direction="row" style={{ minHeight: '100%' }}>
                        <Grid item>
                            <img src="/resources/logo.png" alt="Logo" style={{ minHeight: '50vh' }} />
                        </Grid>
                        <Grid item>
                            <Box m={1} className={`${classes.centerBox} ${classes.box}`} style={{ height: '100%', width: '100%' }}>
                                <Grid container spacing={0} direction="column">
                                    <Grid item style={{ marginBottom: "40px"}}>
                                        <Grid container spacing={0} direction="row" alignItems="center">
                                            <Grid item>
                                                <Typography className={classes.letters} style={{ textAlign: "center" }} >
                                                    Username
                                                </Typography>
                                            </Grid>
                                            <Grid item>
                                                <TextField id="outlined" variant="outlined" style={{width: '40vw', backgroundColor: '#FFFFFF' }}/>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                    <Grid item style={{ marginTop: "40px"}}>
                                        <Grid container spacing={0} direction="row" alignItems="center">
                                            <Grid item>
                                                <Typography className={classes.letters} style={{ textAlign: "center", }} >
                                                    Password
                                                </Typography>
                                            </Grid>
                                            <Grid item> 
                                                <TextField id="outlined-password-input" type="password" autoComplete="current-password" variant="outlined" style={{width: '40vw', backgroundColor: '#FFFFFF', marginLeft: "4px" }}/>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                            </Box>
                        </Grid>     
                    </Grid>
                    </CardContent>
                    <CardActions>
                        <Box component="span" className={`${classes.bottomLeftBox} ${classes.box}`} style={{ height: '100%', width: '100%' }}>
                            <Button variant="contained" style={{ height: '40px', width: '40vw', background: '#FFFFFF', fontSize: "20px", lineHeight: "20px", fontWeight: "bold", color: "gray" }}>
                                Login
                            </Button>
                        </Box>
                    </CardActions>
                </Card>
            </Grid>      
        </Grid>
    );
}