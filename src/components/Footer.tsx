import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';
import RestoreIcon from '@material-ui/icons/Restore';
import FavoriteIcon from '@material-ui/icons/Favorite';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import { Container, Icon, Link, Typography, withStyles } from '@material-ui/core';
import '../componentsCSS/Footer.css';
import { MenuBook, SystemUpdate } from '@material-ui/icons';
import classes from '*.module.css';

const styles = {
    myTextStyle: {
        textDecoration: 'none',
        '&:hover': {
            color: 'white'
        }
    }
};

function PageFooter(props: any) {

    return (
        <footer className="containerFooter" style={{ backgroundColor: '#73ADBE' }}>
            <div style={{ display: 'flex', float: 'right', marginTop: '30px', marginBottom: '30px' }} >
                <MenuBook style={{ fontSize: 40, marginRight: '10px', color: 'white' }} color="inherit" />
                <Link underline='none' href="#">
                    <Typography className={props.classes.myTextStyle} style={{ fontSize: 30, marginRight: '50px', color: 'white' }}>
                        Manual
                    </Typography>
                </Link>
                <SystemUpdate style={{ fontSize: 40, marginRight: '10px', color: 'white' }} color="inherit" />
                <Link underline='none' href="#">
                    <Typography className={props.classes.myTextStyle} style={{ fontSize: 30, marginRight: '50px', color: 'white' }}>
                        Application
                    </Typography>
                </Link>
            </div>
        </footer>
    );
}

export default withStyles(styles)(PageFooter);
