import * as React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import '../componentsCSS/Header.css';
import { Grid, Link } from '@material-ui/core';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    }),
);

const toolbar = {
    background: '#73ADBE',
};

export default function ButtonAppBar() {
    const classes = useStyles();

    return (
        <AppBar position="static" style={{ backgroundColor: '#73ADBE' }}>
            <Toolbar>
                <img src="/resources/logo.png" alt="Logo" className="logo" />
                <Grid container spacing={1}>
                    <Grid item xs={2} spacing={3}>
                        <Link color="inherit" href="#"> 
                        <Typography style={{ textAlign: "center", }} >
                            Beacons
                        </Typography>
                    </Link>
                    </Grid>
                    <Grid item xs={2} spacing={3}>
                    <Link color="inherit" href="#"> 
                        <Typography style={{ textAlign: "center", }} >
                            Classrooms
                        </Typography>
                    </Link>
                    </Grid>
                    <Grid item xs={2} spacing={3}>
                    <Link color="inherit" href="#"> 
                        <Typography style={{ textAlign: "center", }} >
                            Teacher Office
                        </Typography>
                    </Link>
                    </Grid>
                    <Grid item xs={2} spacing={3}>
                    <Link color="inherit" href="#"> 
                        <Typography style={{ textAlign: "center", }} >
                            Entry Points
                        </Typography>
                    </Link>
                    </Grid>
                    <Grid item xs={2} spacing={3}>
                    <Link color="inherit" href="#"> 
                        <Typography style={{ textAlign: "center", }} >
                            Feedback
                        </Typography>
                    </Link>
                    </Grid>
                    <Grid item xs={2} spacing={3}>
                    <Link color="inherit" href="#"> 
                        <Typography style={{ textAlign: "center", }} >
                            Accounts
                        </Typography>
                    </Link>
                    </Grid>
                </Grid>
                <Button color="inherit">Login</Button>
            </Toolbar>
        </AppBar>
    );
}
